<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Admin',
            'email' => 'admin@soundofnews.fr',
            'password' => bcrypt('admin'),
        ]);

        factory(App\User::class, 50)->create();
        factory(App\Category::class, 15)->make()->each(function (App\Category $category){

            $category->user()->associate(App\User::inRandomOrder('')->first())->save();

        });

        factory(App\Tag::class, 10)->make()->each(function (App\Tag $tag){

            $tag->user()->associate(App\User::inRandomOrder('')->first())->save();

        });;

        factory(App\Article::class, 50)->make()->each(function (App\Article $article){

            $article->category()->associate(App\Category::inRandomOrder('')->first());

            $article->user()->associate(App\User::inRandomOrder('')->first())->save();

            $article->tags()->attach(App\Tag::inRandomOrder('')->first());

            $article->save();

        });
        factory(App\Comment::class, 60)->make()->each(function (App\Comment $comment){

            $comment->article()->associate(App\Article::inRandomOrder('')->first());

            $comment->user()->associate(App\User::inRandomOrder('')->first())->save();

            $comment->save();

        });
        factory(App\Thread::class, 10)->make()->each(function (App\Thread $thread){
            $thread->user()->associate(App\User::inRandomOrder('')->first())->save();
        });

        foreach ((range(1, 20)) as $index) {
            DB::table('reports')->insert(
                [
                    'user_id' => App\User::inRandomOrder('')->first()->id,
                    'reportable_id' => rand(0, 1) == 1 ? App\Thread::inRandomOrder('')->first()->id : App\Article::inRandomOrder('')->first()->id,
                    'reportable_type' => rand(0, 1) == 1 ? 'App\Article' : 'App\Thread'
                ]
            );
        }
        factory(App\Message::class, 50)->make()->each(function (App\Message $message){
            $message->user()->associate(App\User::inRandomOrder('')->first())->save();
            $message->thread()->associate(App\Thread::inRandomOrder('')->first())->save();
        });
    }
}
