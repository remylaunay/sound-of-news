<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRightRoleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('right_role', function (Blueprint $table) {
            $table->primary(['role_id','right_id']);
            $table->bigInteger('role_id')->unsigned();
            $table->bigInteger('right_id')->unsigned();
            $table->foreign('role_id')->references('id')->on('roles');
            $table->foreign('right_id')->references('id')->on('rights');
            $table->timestamps();
            $table->softDeletes();
        });
        DB::insert('insert into right_role (role_id, right_id) values (1, 1),(1, 2),(1, 3),(1, 4)');
        DB::insert('insert into right_role (role_id, right_id) values (2, 1),(2, 2),(2, 3),(2, 4)');
        DB::insert('insert into right_role (role_id, right_id) values (3, 1),(3, 4)');
        DB::insert('insert into right_role (role_id, right_id) values (4, 4)');


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('right_role');
    }
}
