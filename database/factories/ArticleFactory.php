<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Article;
use Faker\Generator as Faker;

$factory->define(Article::class, function (Faker $faker) {
    return [
        'title' => $faker->word(),
        'slug' => $faker->slug(),
        'premium' => $faker->boolean(),
        'content' => $faker->realText(),
        'published' => $faker->boolean(),
    ];
});
