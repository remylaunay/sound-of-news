@extends('layouts.manage', ['title' => 'Tags'])
@section('contentPage')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Tags</h4>
                    <div class="table-responsive">
                        <table id="zero_config" class="table table-striped table-bordered no-wrap">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Titre</th>
                                <th>Créateur</th>
                                <th>Dernière modification</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($tags as $tag)

                                <tr>
                                    <td>{{$tag->id}}</td>
                                    <td><b>{{$tag->title}}</b></td>
                                    <td>{{$tag->user->name}}</td>
                                    <td>{{$tag->updated_at}}</td>
                                </tr>

                            @endforeach

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
