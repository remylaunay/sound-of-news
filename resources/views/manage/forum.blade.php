@extends('layouts.manage', ['title' => 'Forum'])
@section('contentPage')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Liste des sujets</h4>
                    <div class="table-responsive">
                        <table id="zero_config" class="table table-striped table-bordered no-wrap">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Titre</th>
                                <th>Créateur</th>
                                <th>Dernière modification</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($threads as $thread)

                                <tr>
                                    <td>{{$thread->id}}</td>
                                    <td><b>{{$thread->title}}</b></td>
                                    <td>{{$thread->user->name}}</td>
                                    <td>{{$thread->updated_at}}</td>
                                </tr>

                            @endforeach

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
