@extends('layouts.manage', ['title' => 'Catégories'])
@section('contentPage')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Catégories</h4>
                    <div class="table-responsive">
                        <table id="zero_config" class="table table-striped table-bordered no-wrap">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Titre</th>
                                <th>Créateur</th>
                                <th>Dernière modification</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($categories as $category)

                            <tr>
                                <td>{{$category->id}}</td>
                                <td><b>{{$category->title}}</b></td>
                                <td>{{$category->user->name}}</td>
                                <td>{{$category->updated_at}}</td>
                            </tr>

                            @endforeach

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
