@extends('layouts.manage', ['title' => 'Articles'])
@section('contentPage')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Articles</h4>
                    <div class="table-responsive">
                        <table id="zero_config" class="table table-striped table-bordered no-wrap">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Titre</th>
                                <th>Catégorie</th>
                                <th>Premium</th>
                                <th>Créateur</th>
                                <th>Publié</th>
                                <th>Dernière modification</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($articles as $article)

                                <tr>
                                    <td>{{$article->id}}</td>
                                    <td><b>{{$article->title}}</b></td>
                                    <td>{{$article->category->title}}</td>
                                    <td>{{($article->premium) ? 'Oui' : 'Non'}}</td>
                                    <td>{{$article->user->name}}</td>
                                    <td>{{($article->published) ? 'Oui' : 'Non'}}</td>
                                    <td>{{$article->updated_at}}</td>
                                </tr>

                            @endforeach

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
