@extends('layouts.manage', ['title' => 'Accueil'])
@section('contentPage')
<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->
<!-- basic table -->
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <!-- Column -->
                    <div class="col-md-6 col-lg-2 col-xlg-2">
                        <div class="card card-hover">
                            <div class="p-2 bg-primary text-center">
                                <h1 class="font-light text-white">{{$users}}</h1>
                                <h6 class="text-white">Utilisateurs</h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-2 col-xlg-2">
                        <div class="card card-hover">
                            <div class="p-2 bg-primary text-center">
                                <h1 class="font-light text-white">{{$categories}}</h1>
                                <h6 class="text-white">Catégories</h6>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <div class="col-md-6 col-lg-2 col-xlg-2">
                        <div class="card card-hover">
                            <div class="p-2 bg-cyan text-center">
                                <h1 class="font-light text-white">{{$articles}}</h1>
                                <h6 class="text-white">Articles</h6>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <div class="col-md-6 col-lg-2 col-xlg-2">
                        <div class="card card-hover">
                            <div class="p-2 bg-success text-center">
                                <h1 class="font-light text-white">{{$tags}}</h1>
                                <h6 class="text-white">Tags</h6>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <div class="col-md-6 col-lg-2 col-xlg-2">
                        <div class="card card-hover">
                            <div class="p-2 bg-danger text-center">
                                <h1 class="font-light text-white">{{ $comments }}</h1>
                                <h6 class="text-white">Commentaires</h6>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <div class="col-md-6 col-lg-2 col-xlg-2">
                        <div class="card card-hover">
                            <div class="p-2 bg-danger text-center">
                                <h1 class="font-light text-white">X</h1>
                                <h6 class="text-white">Sujets sur le forum</h6>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                </div>
                <h3>Dernières actions</h3>
                <div class="table-responsive">
                    <table id="zero_config" class="table table-striped table-bordered no-wrap">
                        <thead>
                        <tr>
                            <th>Element#ID</th>
                            <th>Action</th>
                            <th>Cible</th>
                            <th>Utilisateur</th>
                            <th>Date</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($elements as $element)
                        <tr>
                            <td><span class="badge badge-light-warning">{{$element->target}}#{{$element->id}}</span></td>
                            <td><a class="font-bold link">{{$element->action}}</a></td>
                            <td><a href="/{{Str::lower(Str::plural($element->target))}}/{{$element->elmt_id}}" class="font-weight-medium link">
                            {{$element->target}}
                            <td>{{$element->user->name}}</td>
                            <td>{{$element->created_at}}</td>
                        </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>Element#ID</th>
                            <th>Action</th>
                            <th>Cible</th>
                            <th>Utilisateur</th>
                            <th>Date</th>
                        </tr>
                        </tfoot>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
@stop
