<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="/adminui/assets/images/favicon.png">
    <title>Adminmart Template - The Ultimate Multipurpose admin template</title>
    <!-- This page css -->
    <!-- Custom CSS -->
    <link href="/adminui/dist/css/style.min.css" rel="stylesheet">
<link href="/adminui/assets/extra-libs/datatables.net-bs4/css/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>

    <![endif]-->
</head>

<body>
<!-- ============================================================== -->
<!-- Preloader - style you can find in spinners.css -->
<!-- ============================================================== -->
<div class="preloader">
    <div class="lds-ripple">
        <div class="lds-pos"></div>
        <div class="lds-pos"></div>
    </div>
</div>
<!-- ============================================================== -->
<!-- Main wrapper - style you can find in pages.scss -->
<!-- ============================================================== -->
<div id="main-wrapper" data-theme="light" data-layout="vertical" data-navbarbg="skin6" data-sidebartype="full" data-sidebar-position="fixed" data-header-position="fixed" data-boxed-layout="full">
    <!-- ============================================================== -->
    <!-- Topbar header - style you can find in pages.scss -->
    <!-- ============================================================== -->
@include('layouts.manage_head')
<!-- ============================================================== -->
    <!-- End Topbar header -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Left Sidebar - style you can find in sidebar.scss  -->
    <!-- ============================================================== -->
    <aside class="left-sidebar" data-sidebarbg="skin6">
        <!-- Sidebar scroll-->
        <div class="scroll-sidebar" data-sidebarbg="skin6">
            <!-- Sidebar navigation-->
            <nav class="sidebar-nav">
                <ul id="sidebarnav">
                    <li class="sidebar-item {{ (request()->is('manage')) ? 'selected' : '' }}">
                        <a class="sidebar-link sidebar-link" href="{{route('manage.index')}}"
                                                 aria-expanded="false"><i data-feather="home" class="feather-icon"></i><span
                                class="hide-menu">Accueil</span></a></li>
                    <li class="list-divider"></li>
                    <li class="sidebar-item {{ (request()->segment(2) == 'content') ? 'selected' : '' }}">
                        <a class="sidebar-link has-arrow {{ (request()->is('manage/content*')) ? 'selected active' : '' }}" href="javascript:void(0)"
                                                 aria-expanded="false"><i data-feather="grid" class="feather-icon"></i><span
                                class="hide-menu">Contenu </span></a>
                        <ul aria-expanded="false" class="collapse  first-level base-level-line {{(request()->segment(2) == 'content') ? 'in' : '' }}">
                            <li class="sidebar-item {{ (request()->is('manage/content/categories')) ? 'active' : '' }}"><a href="{{route('manage.content_categories')}}" class="sidebar-link {{ (request()->is('manage/content/categories')) ? 'active' : '' }}"><span
                                        class="hide-menu"> Catégories
                                        </span></a>
                            </li>
                            <li class="sidebar-item  {{ (request()->is('manage/content/articles')) ? 'active' : '' }}"><a href="{{route('manage.content_articles')}}" class="sidebar-link {{ (request()->is('manage/content/articles')) ? 'active' : '' }}"><span
                                        class="hide-menu"> Articles
                                        </span></a>
                            </li>
                            <li class="sidebar-item  {{ (request()->is('manage/content/tags')) ? 'active' : '' }}"><a href="{{route('manage.content_tags')}}" class=" {{ (request()->is('manage/content/tags')) ? 'active' : '' }} sidebar-link"><span
                                        class="hide-menu"> Tags
                                        </span></a>
                            </li>

                        </ul>
                    </li>
                    <li class="sidebar-item {{ (request()->is('manage/reports')) ? 'active' : '' }}"> <a class="sidebar-link" href="{{route('manage.reports')}}"
                                                 aria-expanded="false"><i data-feather="tag" class="feather-icon"></i><span
                                class="hide-menu">Signalements
                                </span></a>
                    </li>
                    <li class="sidebar-item {{ (request()->is('manage/forum')) ? 'active' : '' }}"> <a class="sidebar-link" href="{{route('manage.forum')}}"
                                                                                                         aria-expanded="false"><i data-feather="message-square" class="feather-icon"></i><span
                                class="hide-menu">Forum
                                </span></a>
                    </li>

                    <li class="list-divider"></li>

                    <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="#"
                                                 aria-expanded="false"><i data-feather="log-out" class="feather-icon"></i><span
                                class="hide-menu">Se déconnecter</span></a></li>
                </ul>
            </nav>
            <!-- End Sidebar navigation -->
        </div>
        <!-- End Sidebar scroll-->
    </aside>
    <!-- ============================================================== -->
    <!-- End Left Sidebar - style you can find in sidebar.scss  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Page wrapper  -->
    <!-- ============================================================== -->
    <div class="page-wrapper">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="page-breadcrumb">
            <div class="row">
                <div class="col-7 align-self-center">
                    <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">{{$title}}</h4>
                    <div class="d-flex align-items-center">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb m-0 p-0">
                                <li class="breadcrumb-item"><a href="/adminui/html/index.html" class="text-muted">Management</a></li>
                                <li class="breadcrumb-item text-muted active" aria-current="page">{{ $title }}</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Container fluid  -->
        <!-- ============================================================== -->
        <div class="container-fluid">

            @yield('contentPage')
        <!-- ============================================================== -->
        <!-- End Container fluid  -->
        <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- footer -->
        <!-- ============================================================== -->

                <footer class="footer text-center text-muted">
            All Rights Reserved by Adminmart. Designed and Developed by <a
                href="https://wrappixel.com">WrapPixel</a>.
        </footer>
        <!-- ============================================================== -->
        <!-- End footer -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Page wrapper  -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Wrapper -->
<!-- ============================================================== -->
<!-- End Wrapper -->
<!-- ============================================================== -->
<!-- All Jquery -->
<!-- ============================================================== -->

<script src="/adminui/assets/libs/jquery/dist/jquery.min.js"></script>
    <script src="/adminui/assets/extra-libs/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="/adminui/dist/js/pages/datatable/datatable-basic.init.js"></script>
<script src="/adminui/assets/libs/popper.js/dist/umd/popper.min.js"></script>
<script src="/adminui/assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- apps -->
<script src="/adminui/dist/js/app-style-switcher.js"></script>
<script src="/adminui/dist/js/feather.min.js"></script>
<script src="/adminui/assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
<script src="/adminui/dist/js/sidebarmenu.js"></script>
<!--Custom JavaScript -->
<script src="/adminui/dist/js/custom.min.js"></script>
</body>

</html>
