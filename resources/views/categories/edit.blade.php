@extends('layouts.website')

@section('contentPage')
    <div class="col-12">
        <h1>Modification</h1>
        <div class="form-group">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            {!! Form::model($category,["route" => array('categories.update',$category),"method" => "put"]) !!}

            {!! Form::label('title', 'Titre') !!}
            {!! Form::text('title', null, ['class' => 'form-control']) !!}

            {!! Form::label('slug', 'Slug') !!}
            {!! Form::text('slug', null,  ['class' => 'form-control']) !!}

            {!! Form::submit('Modifier', ['class' => 'btn btn-primary']) !!}
            {!! Form::close() !!}
        </div>
    </div>
@stop
