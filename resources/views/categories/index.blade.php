@extends('layouts.website')

@section('contentPage')
    <div class="col-12">
        <h1>Catégories</h1>
        <div class="categoroes col-12 row">
            @foreach ($categories as $category)
                <article style="height:200px;margin-top:60px;" class="col-lg-4">
                <a style="color: black;"href="{{route('categories.show',['category' => $category->id])}}"><h2 style="border-bottom:1px solid #749291;font-size:1.2em; text-align:center;padding-bottom: 10px;">{{$category->title}}</h2></a>
                <p style="min-height:70px;"l>{{substr($category->description,0,100)}}</p>

                @auth
                    @if (Auth::user()->hasRight('UPDATE'))
                        {!! Form::open(array('route' => array('categories.edit', $category->id),'method' => 'get','style'=>'display:inline-block')) !!}
                        {!! Form::submit('Modifier', ['class' => 'btn btn-primary']) !!}
                        {!! Form::close() !!}
                    @endif
                    @if (Auth::user()->hasRight('DELETE'))
                        {!! Form::open(array('route' => array('categories.destroy', $category->id),'method' => 'delete', 'style'=>'display:inline-block')) !!}
                        {!! Form::submit('Supprimer', ['class' => 'btn btn-primary']) !!}
                        {!! Form::close() !!}
                    @endif
                @endauth
                </article>
            @endforeach
        </div>
    </div>
@stop
