@extends('layouts.website')


@section('contentPage')
    <div class="col-12">
        <div class="categories">
            <h1>{{$tag->title}}</h1>
            <h2>Slug : {{$tag->slug}}</h2>
        </div>
        {!! Form::model($tag,["route" => array('tags.destroy',$tag), "method" => "delete"]) !!}
        {!! Form::submit("Supprimer",['class' => 'btn btn-primary','style' => 'display:inline-block']) !!}
        {!! Form::close() !!}

        {!! Form::model($tag,["route" => array('tags.edit',$tag), "method" => "get"]) !!}
        {!! Form::submit("Modifier",['class' => 'btn btn-primary', 'style' => 'display:inline-block']) !!}
        {!! Form::close() !!}
        <div class="tags col-12 row">
        @foreach ($tag->articles as $article)
            <article style="height:200px;margin-top:60px;" class="col-lg-4">
            <a style="color: black;"href="{{route('articles.show',['article' => $article->id])}}"><h2 style="border-bottom:1px solid #749291;font-size:1.2em; text-align:center;padding-bottom: 10px;">{{$article->title}}</h2></a>
            <p style="min-height:70px;"l>{{substr($article->content,0,100)}}</p>

            <ul>
                <li>Catégorie :  <a href="{{ route('categories.show',['category' => $article->category->id]) }}"> {{ $article->category->title }} </a></li>
                <li>Article créé le : {{$article->created_at->format('D d M Y')}}</li>
                <li>Article modifié le : {{$article->updated_at->format('D d M Y')}}</li>
            </ul>
            {!! Form::open(array('route' => array('articles.update', $article->id),'method' => 'get','style'=>'display:inline-block')) !!}
            {!! Form::submit('Modifier', ['class' => 'btn btn-primary']) !!}
            {!! Form::close() !!}
            {!! Form::open(array('route' => array('articles.destroy', $article->id),'method' => 'delete', 'style'=>'display:inline-block')) !!}
            {!! Form::submit('Supprimer', ['class' => 'btn btn-primary']) !!}
            {!! Form::close() !!}
            </article>
        @endforeach
        </div>
    </div>
@stop
