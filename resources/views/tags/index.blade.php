@extends('layouts.website')


@section('contentPage')
    <div class="col-12">
        <h1>Tags</h1>
        <div class="categoroes col-12 row">
            @foreach ($tags as $tag)
                <article style="height:200px;margin-top:60px;" class="col-lg-4">
                <a style="color: black;"href="{{route('tags.show',['category' => $tag->id])}}"><h2 style="border-bottom:1px solid #749291;font-size:1.2em; text-align:center;padding-bottom: 10px;">{{$tag->title}}</h2></a>

                @auth
                    @if (Auth::user()->hasRight('UPDATE'))
                        {!! Form::open(array('route' => array('tags.edit', $tag->id),'method' => 'get','style'=>'display:inline-block')) !!}
                        {!! Form::submit('Modifier', ['class' => 'btn btn-primary']) !!}
                        {!! Form::close() !!}
                    @endif
                    @if (Auth::user()->hasRight('UPDATE'))
                        {!! Form::open(array('route' => array('tags.destroy', $tag->id),'method' => 'delete', 'style'=>'display:inline-block')) !!}
                        {!! Form::submit('Supprimer', ['class' => 'btn btn-primary']) !!}
                        {!! Form::close() !!}
                    @endif
                @endauth
                </article>
            @endforeach
        </div>
    </div>
@stop
