@extends('layouts.website')

@section('contentPage')
        <div class="col-12">
            <h1>Création</h1>
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            {!! Form::open(array('route' => array('tags.store'),'method' => 'post') )!!}
            <div class="form-group">
                    {!! Form::label('title', 'Titre') !!}
                    {!! Form::text('title', null, ['class' => 'form-control']) !!}


                    {!! Form::submit('Créer', ['class' => 'btn btn-primary']) !!}
            </div>
            {!! Form::close() !!}
        </div>
@stop
