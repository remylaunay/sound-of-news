@extends('layouts.website')

@section('contentPage')
        <div class="col-12">
            <h1>Modification</h1>
            <div class="form-group">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                {!! Form::model($article,["route" => array('articles.update',$article),"method" => "put"]) !!}

                {!! Form::label('title', 'Titre') !!}
                {!! Form::text('title', null, ['class' => 'form-control']) !!}

                {!! Form::label('content', 'Contenu') !!}
                {!! Form::textarea('content', null, ['class' => 'form-control']) !!}

                {!! Form::label('slug', 'Slug') !!}
                {!! Form::text('slug', null,  ['class' => 'form-control']) !!}

                {!! Form::label('category_id', 'Catégorie') !!}
                {!! Form::select('category_id',$categories,null,['class' => 'form-control']) !!}


                    {!! Form::label('tags[]', 'Tags') !!}
                    {!! Form::select('tags[]',$tags,null,['class' => 'form-control','multiple' => 'multiple']) !!}

                    {!! Form::select('premium', array('0' => 'Public', '1' => 'Premium'),null,['class' => 'form-control']) !!}
                    {!! Form::select('published', array('0' => 'Non publié', '1' => 'Publié'),null,['class' => 'form-control']) !!}

        {!! Form::submit('Modifier', ['class' => 'btn btn-primary']) !!}
            {!! Form::close() !!}
            </div>
    </div>
@stop
