@extends('layouts.website')

@section('contentPage')
        <div class="col-12">
            <div class="articles">
                <h1>{{$article->title}}</h1>
                <h2>Catégorie : {{$article->category->title}}</h2>
                <h2>Tags :
                    <ul>
                        @foreach ($article->tags as $tag)
                            <li>{{ $tag->title }}</li>
                        @endforeach
                    </ul></h2>
                    <p>{{$article->content}}</p>
            </div>
            {!! Form::model($article,["method" => "delete"]) !!}
              {!! Form::submit("Supprimer",['class' => 'btn btn-primary']) !!}
            {!! Form::close() !!}
        </div>
@stop
