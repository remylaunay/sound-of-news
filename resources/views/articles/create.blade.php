@extends('layouts.website')

@section('contentPage')
        <div class="col-12">
            <h1>Création</h1>
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            {!! Form::open(array('route' => array('articles.store'),'method' => 'post') )!!}
            <div class="form-group">
                    {!! Form::label('title', 'Titre') !!}
                    {!! Form::text('title', null, ['class' => 'form-control']) !!}

                    {!! Form::label('content', 'Contenu') !!}
                    {!! Form::textarea('content', null, ['class' => 'form-control']) !!}
                    {!! Form::label('category_id', 'Catégorie') !!}
                    {!! Form::select('category_id',$categories,null,['class' => 'form-control']) !!}
                {!! Form::label('tags[]', 'Tags') !!}
                {!! Form::select('tags[]',$tags,null,['class' => 'form-control','multiple' => 'multiple']) !!}
                {!! Form::select('premium', array('0' => 'Public', '1' => 'Premium'),null,['class' => 'form-control']) !!}
                {!! Form::select('published', array('0' => 'Non publié', '1' => 'Publié'),null,['class' => 'form-control']) !!}
                    {!! Form::submit('Créer', ['class' => 'btn btn-primary']) !!}
            </div>
            {!! Form::close() !!}
        </div>
@stop
