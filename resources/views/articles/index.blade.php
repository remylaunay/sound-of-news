@extends('layouts.website')

@section('contentPage')
    <div class="col-12">
        <h1>Articles</h1>
        <div class="col-12 mt-5">
            <div class="row justify-content-center">
                <span class="text-center align-middle">{{$articles->links()}}</span>
            </div>
        </div>
        <div class="articles col-12 row">
            @foreach ($articles as $article)
                <article style="height:200px;margin-top:70px;" class="col-lg-4">
                    <a style="color: black;"href="{{route('articles.show',['article' => $article->id])}}"><h2 style="border-bottom:1px solid #749291;font-size:1.2em; text-align:center;padding-bottom: 10px;">{{$article->title}}</h2></a>
                    <p style="min-height:70px;"l>{{substr($article->content,0,100)}}</p>

                    <ul>
                      <li>Catégorie :  <a href="{{ route('categories.show',['category' => $article->category->id])}}"> {{ $article->category->title }} </a></li>
                        <li>Article créé le : {{$article->created_at->format('D d M Y')}}</li>
                        <li>Article modifié le : {{$article->updated_at->format('D d M Y')}}</li>
                    </ul>
                    @auth
                        @if (Auth::user()->hasRight('UPDATE'))
                            {!! Form::open(array('route' => array('articles.edit', $article->id),'method' => 'get','style'=>'display:inline-block')) !!}
                                {!! Form::submit('Modifier', ['class' => 'btn btn-primary']) !!}
                            {!! Form::close() !!}
                        @endif
                        @if (Auth::user()->hasRight('DELETE'))
                            {!! Form::open(array('route' => array('articles.destroy', $article->id),'method' => 'delete', 'style'=>'display:inline-block')) !!}
                                {!! Form::submit('Supprimer', ['class' => 'btn btn-primary']) !!}
                            {!! Form::close() !!}
                        @endif
                    @endauth
                </article>
            @endforeach
        </div>
    </div>
    <div class="col-12 mt-5">
        <div class="row mt-5 justify-content-center">
            <span class="text-center align-middle mt-5">{{$articles->links()}}</span>
        </div>
    </div>
@stop
