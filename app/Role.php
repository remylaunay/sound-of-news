<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class Role extends Model
{
    protected $fillable = [
        'name',
    ];
    public function setNameAttribute($value)
    {
        $this->attributes['name'] = $value;
    }
    public function rights()
    {
        return $this->belongsToMany('App\Right');
    }
}
