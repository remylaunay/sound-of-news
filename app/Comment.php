<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = [
        'content',
        'active',
    ];
    public function article(){
        return $this->belongsTo(Article::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function reports()
    {
        return $this->morphMany('App\Report', 'reportable');
    }
//   public function commentable()
//    {
//        return $this->morphTo();
//    }
//A voir plus tard
}
