<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Action extends Model
{
protected $fillable = ["action","target","user_id","elmt_id"];

    public function user(){
            return $this->belongsTo(User::class);
    }

}
