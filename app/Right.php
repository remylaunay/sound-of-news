<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Right extends Model
{
    protected $fillable = [
        'name',
    ];
    //
    public function role()
    {
        return $this->belongsToMany(Role::class);
    }
    public static function hasRight($right){
       return Right::name == $right;
    }
}
