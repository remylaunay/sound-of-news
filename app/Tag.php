<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class Tag extends Model
{
    protected $fillable = [
        'title',
    ];
    public function setTitleAttribute($value)
    {
        $this->attributes['title'] = $value;
        $this->attributes['slug'] = Str::slug($value,'-');
        $this->attributes['user_id'] = Auth::id();
    }
    public function articles(){
        return $this->belongsToMany(Article::class);
    }
    public function user(){
        return $this->belongsTo(User::class);
    }
}
