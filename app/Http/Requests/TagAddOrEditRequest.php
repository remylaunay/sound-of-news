<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;


class TagAddOrEditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:50',
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'Merci de renseigner le titre',
            'title.max' => 'Le titre ne peut dépasser 50 caractères',
         ];
    }
}
