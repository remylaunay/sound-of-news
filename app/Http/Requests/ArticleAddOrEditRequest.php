<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;


class ArticleAddOrEditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:100',
            'content' => 'required|min:20',
            'category_id' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'Merci de renseigner le titre',
            'title.max' => 'Le titre ne peut dépasser 100 caractères',
            'content.required' => 'Le contenu est requis',
            'content.min' => 'Le contenu doit contenir 20 caractères minimum',
            'slug.required' => 'Merci de renseigner un slug',
            'category_id.required' => 'Il est obligatoire de renseigner une catégorie',
        ];
    }
}
