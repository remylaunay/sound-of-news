<?php

namespace App\Http\Middleware;

use Closure;

class CheckIP
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $whitelist = ['127.0.0.1','10.2.15.32','10.2.30.21'];
        if(in_array($request->getClientIp(), $whitelist)){
            return $next($request);
        }
        return abort(403, 'Unauthorized action.');
    }



}
