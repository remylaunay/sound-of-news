<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;

use Closure;

class CheckRoles
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @param $right
     * @return mixed
     */
    public function handle($request, Closure $next, $right)
    {
        if (!Auth::check()) // I included this check because you have it, but it really should be part of your 'auth' middleware, most likely added as part of a route group.
            return abort('403');

        $user = $request->user();

        if($user->hasRight($right)) {
            return $next($request);
        }


        return abort('403');

    }
}
