<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
use App\Category;
use App\Tag;
use App\Comment;
use App\User;
use App\Action;
use App\Thread;
use App\Message;
use App\Report;

class ManageController extends Controller
{
    public function index(){
        $articles = count(Article::All());
        $tags = count(Tag::All());
        $categories = count(Category::All());
        $comments = count(Comment::All());
        $users = count(User::All());
        $elements = Action::limit(15)
            ->orderBy('created_at', 'desc')
            ->get();
        return view('manage.index',["categories" => $categories,"articles" => $articles, "tags" => $tags,"users" => $users,"comments" => $comments,"elements" => $elements]);
    }

    public function categories(){
        $data = Category::All();
        return view('manage.content_categories',["categories" => $data]);
    }
    public function articles(){
        $data = Article::All();
        return view('manage.content_articles',["articles" => $data]);
    }
    public function tags(){
        $data = Tag::All();
        return view('manage.content_tags',["tags" => $data]);
    }
    public function users(){
        $data = User::All();
        return view('manage.users',["users" => $data]);
    }
    public function forum(){
        $data = Thread::All();
        return view('manage.forum',["threads" => $data]);
    }
    public function reports(){
        $data = Thread::All();
        return view('manage.reports',["reports" => $data]);
    }
}
