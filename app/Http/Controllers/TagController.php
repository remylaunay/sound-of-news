<?php

namespace App\Http\Controllers;

use App\Action;
use App\Events\NewAction;
use App\Tag;
use App\Http\Requests\TagAddOrEditRequest;
use Illuminate\Http\Request;
use Illuminate\View\Middleware\ShareErrorsFromSession;

class TagController extends Controller
{
    public function __construct()
    {
        $this->middleware('check.roles:CREATE,UPDATE,DELETE')->except(['index','show']);
        $this->middleware('check.roles:CREATE')->only(['create','store']);
        $this->middleware('check.roles:UPDATE')->only(['edit','update']);
        $this->middleware('check.roles:DELETE')->only(['destroy']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tags = Tag::With('articles')->get();
        return view('tags.index', compact("tags"));
    }


    public function create()
    {
        return view('tags.create');
    }

    public function store(TagAddOrEditRequest $request)
    {
        $elmt = new Tag($request->all());
        $elmt->save();
        event(new NewAction(new Action(['target' => strtoupper(substr(get_class(),21,-10)), 'action' => strtoupper(__FUNCTION__), 'user_id' => $request->user()->id,'elmt_id' => $elmt->id])));
        return redirect()->route('tags.index');
    }

    public function show(Tag $tag)
    {
        return view('tags.show', ['tag' => $tag]);
    }

    public function edit(Tag $tag)
    {
        return view('tags.edit', ['tag' => $tag]);
    }

    public function update(TagAddOrEditRequest $request, Tag $elmt)
    {
        $elmt->update($request->all());
        event(new NewAction(new Action(['target' => strtoupper(substr(get_class(),21,-10)), 'action' => strtoupper(__FUNCTION__), 'user_id' => $request->user()->id,'elmt_id' => $elmt->id])));
        return redirect()->route('tags.show', ['tag' => $elmt]);
    }

    public function destroy(Request $request, int $elmt)
    {
        event(new NewAction(new Action(['target' => strtoupper(substr(get_class(),21,-10)), 'action' => strtoupper(__FUNCTION__), 'user_id' => $request->user()->id,'elmt_id' => $elmt])));
        Tag::destroy($elmt);
        return redirect()->route('tags.index');
    }
}
