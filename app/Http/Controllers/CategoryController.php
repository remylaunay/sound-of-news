<?php

namespace App\Http\Controllers;

use App\Action;
use App\Category;
use App\Events\NewAction;
use App\Http\Requests\CategoryAddOrEditRequest;
use Illuminate\Http\Request;
use Illuminate\View\Middleware\ShareErrorsFromSession;

class CategoryController extends Controller
{

    /**
     * CategoryController constructor.
     */
    public function __construct()
    {
        $this->middleware('check.roles:CREATE,UPDATE,DELETE')->except(['index','show']);
        $this->middleware('check.roles:CREATE')->only(['create','store']);
        $this->middleware('check.roles:UPDATE')->only(['edit','update']);
        $this->middleware('check.roles:DELETE')->only(['destroy']);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::With('articles')->get();
        return view('categories.index', compact("categories"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryAddOrEditRequest $request)
    {
        $elmt = new Category($request->all());
        $elmt->save();
        event(new NewAction(new Action(['target' => strtoupper(substr(get_class(),21,-10)), 'action' => strtoupper(__FUNCTION__), 'user_id' => $request->user()->id,'elmt_id' => $elmt->id])));
        return redirect()->route('categories.index');
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        return view('categories.show', ['category' => $category]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        return view('categories.edit', ['category' => $category]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryAddOrEditRequest $request, Category $elmt)
    {
        $elmt->update($request->all());
        event(new NewAction(new Action(['target' => strtoupper(substr(get_class(),21,-10)), 'action' => strtoupper(__FUNCTION__), 'user_id' => $request->user()->id,'elmt_id' => $elmt->id])));
        return redirect()->route('categories.show', ['category' => $elmt]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, int $elmt)
    {
        //dd($elmt);
        event(new NewAction(new Action(['target' => strtoupper(substr(get_class(),21,-10)), 'action' => strtoupper(__FUNCTION__), 'user_id' => $request->user()->id,'elmt_id' => $elmt])));
        Category::destroy($elmt);
        return redirect()->route('categories.index');
    }
}
