<?php

namespace App\Http\Controllers;

use App\Action;
use App\Category;
use App\Events\NewAction;
use App\Tag;

use App\Http\Requests\ArticleAddOrEditRequest;
use App\Http\Requests\ArticleAddRequest;
use Illuminate\Http\Request;
use Illuminate\View\Middleware\ShareErrorsFromSession;
use App\Article;

class ArticleController extends Controller
{
    public function __construct()
    {
        $this->middleware('check.roles:CREATE,UPDATE,DELETE')->except(['index','show']);
        $this->middleware('check.roles:CREATE')->only(['create','store']);
        $this->middleware('check.roles:UPDATE')->only(['edit','update']);
        $this->middleware('check.roles:DELETE')->only(['destroy']);
    }
    public function index()
    {
        $articles = Article::With('category')->paginate(9);
        return view('articles.index', compact("articles"));
    }
    public function show($id)
    {
        //Article::find(id)
        //php artisan tinker -- debug model
        $article = Article::findOrFail($id);
        return view('articles.show', ['article' => $article]);
    }
    public function create()
    {
        $categories = Category::All('id', 'title');
        $tags = Tag::All('id', 'title');
        return view('articles.create', ['categories' => $categories->pluck('title','id'),'tags' => $tags->pluck('title','id')]);
    }

    public function store(ArticleAddOrEditRequest $request)
    {
        $elmt = new Article($request->all());
        $elmt->save();
        $elmt->tags()->attach($request->get('tags'));
        event(new NewAction(new Action(['target' => strtoupper(substr(get_class(),21,-10)), 'action' => strtoupper(__FUNCTION__), 'user_id' => $request->user()->id,'elmt_id' => $elmt->id])));
        return redirect()->route('articles.show', ['article' => $elmt]);
    }

    public function edit(Article $article)
    {
        $tags = Tag::All('id', 'title');
        $categories = Category::all('id', 'title');
        return view('articles.edit', ['article' => $article, 'categories' => $categories->pluck('title','id'), 'tags' => $tags->pluck('title','id')]);
    }
    public function update(ArticleAddOrEditRequest $request,Article $elmt)
    {
        $elmt->update($request->all());
        $elmt->tags()->sync($request->get('tags'));
        event(new NewAction(new Action(['target' => strtoupper(substr(get_class(),21,-10)), 'action' => strtoupper(__FUNCTION__), 'user_id' => $request->user()->id,'elmt_id' => $elmt->id])));
        return redirect()->route('articles.show', ['article' => $elmt]);
    }
    public function destroy(Request $request, Article $elmt)
    {
        event(new NewAction(new Action(['target' => strtoupper(substr(get_class(),21,-10)), 'action' => strtoupper(__FUNCTION__), 'user_id' => $request->user()->id,'elmt_id' => $elmt->id])));
        $elmt->delete($elmt);
        return redirect()->route('articles.index');
    }
}
