<?php

namespace App\Providers;

use App\Events\CreateArticle;
use App\Listeners\NewUser;
use App\Mail\registration;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array1
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
            NewUser::class,
        ],
        'App\Events\CreateArticle' => ['App\Listeners\CreateArticle'],
        'App\Events\NewAction' => ['App\Listeners\NewAction'],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
