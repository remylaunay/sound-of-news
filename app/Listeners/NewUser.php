<?php
namespace App\Listeners;
use Illuminate\Auth\Events\Registered;
use App\Mail\registration;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class NewUser
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function handle($event)
    {
        Mail::send(
            new registration($event->user)
        );
    }
}
