<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('home');
});


Route::get('/home', function () {
    return view('home');
});

Route::group(['middleware' => ['check.roles:CREATE'], 'prefix' => 'manage'],function () {
    Route::get('/', 'ManageController@index')->name('manage.index');
    Route::get('/content/categories', 'ManageController@categories')->name('manage.content_categories');
    Route::get('/content/articles', 'ManageController@articles')->name('manage.content_articles');
    Route::get('/content/tags', 'ManageController@tags')->name('manage.content_tags');
    Route::get('/users', 'ManageController@users')->name('manage.users');
    Route::get('/reports', 'ManageController@reports')->name('manage.reports');
    Route::get('/forum', 'ManageController@forum')->name('manage.forum');
});


Route::resource('categories', 'CategoryController');
Route::resource('articles', 'ArticleController');
Route::resource('tags', 'TagController');

Auth::routes();
